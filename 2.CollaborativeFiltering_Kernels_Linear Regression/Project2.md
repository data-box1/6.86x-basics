# Project 2: Sentiment Analysis

The goal of this project is to design a classifier to use for sentiment analysis of product reviews.

#### 1. Dataset
The MNIST database contains binary images of handwritten digits commonly used to train image processing systems. The digits were collected from among Census Bureau employees and high school students. The database contains 60,000 training digits and 10,000 testing digits, all of which have been size-normalized and centered in a fixed-size image of 28 x 28 pixels

#### 3. Objectives

* classifying these images into the correct digit.

• part1/linear_regression.py where you will implement linear regression
• part1/svm.py where you will implement support vector machine
• part1/softmax.py where you will implement multinomial regression
• part1/features.py where you will implement principal component analysis (PCA) dimensionality
reduction
• part1/kernel.py where you will implement polynomial and Gaussian RBF kernels
• part1/main.py where you will use the code you write for this part of the project


2. Linear Regression with Closed Form Solution