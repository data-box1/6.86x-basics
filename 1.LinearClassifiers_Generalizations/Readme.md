# Project 1: Sentiment Analysis

The goal of this project is to design a classifier to use for sentiment analysis of product reviews.

#### 1. Dataset
 Our training set consists of reviews written by Amazon customers for various food products. The reviews, originally given on a 5 point scale, have been adjusted to a +1 or -1 scale, representing a positive or negative review, respectively. 

Below are two example entries from our dataset. Each entry consists of the review and its label. The two reviews were written by different customers describing their experience with a sugar-free candy.

| Review | Label |
| ------ | ----- |
| Nasty No flavor. The candy is just red, No flavor. Just plan and chewy. I would never buy them again | -1 |
| YUMMY! You would never guess that they're sugar-free and it's so great that you can eat them pretty much guilt free! i was so impressed that i've ordered some for myself (w dark chocolate) to take to the office. These are just EXCELLENT! | 1 |

#### 2. Hinge Loss

In this project you will be implementing linear classifiers beginning with the Perceptron algorithm. You will begin by writing your loss function, a hinge-loss function. For this function you are given the parameters of your model $\theta$ and $\theta_0$ . Additionally, you are given a feature matrix in which the rows are feature vectors and the columns are individual features, and a vector of labels representing the actual sentiment of the corresponding feature vector.

#### 3. Objectives

In order to automatically analyze reviews, you will need to complete the following tasks:

* Implement and compare three types of linear classifiers: the perceptron algorithm, the average perceptron algorithm, and the Pegasos algorithm.

* Use your classifiers on the food review dataset, using some simple text features.

* Experiment with additional features and explore their impact on classifier performance.

#### 5. Mini summary of the results

The results obtained with hyperopt 

```python
# Hyperopt setup vars
search_space = hp.choice('classifier_type', [
    {
        'type': hp.choice('type',['perceptron','perceptron_avg','pegasos']),
        'T': hp.randint('T', 20),
        'L': hp.uniform('L', 0.01,0.4),
    }
])
```

are summarized in the Figs. bellow

![alt text](figs/tracking-server-outup.png "mlFlow tracking server")

![alt text](figs/mlflow-analsyis.png "mlFlow tracking comparison")

The better results were given by perceptron_avg as showed.

#### 5. Todo

* Run in val & test datasets

* Select the best and register the model

* Auto Removing the models according best results

* Serve with fastAPi/React

* check if using same dictionary is ok:

```python 
dictionary = p1.bag_of_words(path_data, train_texts)

train_bow_features = p1.extract_bow_feature_vectors(train_texts, dictionary)
val_bow_features = p1.extract_bow_feature_vectors(val_texts, dictionary)
test_bow_features = p1.extract_bow_feature_vectors(test_texts, dictionary)
```