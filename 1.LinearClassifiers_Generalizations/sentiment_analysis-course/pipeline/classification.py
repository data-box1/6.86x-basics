
import numpy as np
import pipeline.preprocessing as prepro

def classify(feature_matrix, theta, theta_0):
    """
    A classification function that uses theta and theta_0 to classify a set of
    data points.

    Args:
        feature_matrix - A numpy matrix describing the given data. Each row
            represents a single data point.
        theta - A numpy array describing the linear classifier.
        theta_0 - A real valued number representing the offset parameter.

    Returns: A numpy array of 1s and -1s where the kth element of the array is
    the predicted classification of the kth row of the feature matrix using the
    given theta and theta_0. If a prediction is GREATER THAN zero, it should
    be considered a positive classification.
    """
    (nsamples, nfeatures) = feature_matrix.shape
    results = np.zeros(nsamples)
    for i in prepro.get_order(nsamples):
        feature_vector = feature_matrix[i]
        value_full_prec = np.dot(theta,feature_vector)+theta_0
        tol = 1e-8
        value = 0. if abs(value_full_prec - 0.) < tol else value_full_prec
        results[i] = 1. if value > 0. else -1.
    return results

