import numpy as np

def hinge_loss_single(feature_vector, label, theta, theta_0):
    """
    Finds the hinge loss on a single data point given specific classification
    parameters.

    Args:
        feature_vector - A numpy array describing the given data point.
        label - A real valued number, the correct classification of the data
            point.
        theta - A numpy array describing the linear classifier.
        theta_0 - A real valued number representing the offset parameter.


    Returns: A real number representing the hinge loss associated with the
    given data point and parameters.
    # sol from MIT
    #1
    y = np.dot(theta, feature_vector) + theta_0
    loss = max(0.0, 1 - y * label)
    return loss
    # 2 
    y = theta @ feature_vector + theta_0 # Here, the @ operator is shorthand for dot product. 
    return max(0, 1 - y * label)

    """
    feature_vector = np.array(feature_vector, dtype='float')
    theta = np.array(theta, dtype='float')
    z = label*(np.dot(theta, feature_vector) + theta_0)
    
    return (1-z) if z <= 1. else 0. 

def hinge_loss_full(feature_matrix, labels, theta, theta_0):
    """
    Finds the total hinge loss on a set of data given specific classification
    parameters.

    Args:
        feature_matrix - A numpy matrix describing the given data. Each row
            represents a single data point.
        labels - A numpy array where the kth element of the array is the
            correct classification of the kth row of the feature matrix.
        theta - A numpy array describing the linear classifier.
        theta_0 - A real valued number representing the offset parameter.


    Returns: A real number representing the hinge loss associated with the
    given dataset and parameters. This number should be the average hinge
    loss across all of the points in the feature matrix.
    #1
    loss = 0
    for i in range(len(feature_matrix)):
        loss += hinge_loss_single(feature_matrix[i], labels[i], theta, theta_0)
    return loss / len(labels)
    #2
    ys = feature_matrix @ theta + theta_0
    loss = np.maximum(1 - ys * labels, np.zeros(len(labels)))
    return np.mean(loss) 
    """
    
    zs = [labels[i]*(np.dot(theta, feature_matrix[i]) + theta_0) for i in range(len(labels))]
    hinges = [ (1-zs[i]) if zs[i] <= 1. else 0. for i in range(len(labels))]
    return np.average(hinges)



# Part II
